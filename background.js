var partials;

chrome.omnibox.onInputChanged.addListener(function(text) {    
    var disc, s;
    partials = []
    
    if (!text) return;
    s = text.split(" ");
    
    for (var i in localStorage) if ((i.length >= s[0].length) && (i.substring(0, s[0].length) == s[0])) partials.push(i);

    if (partials.length>0) {
        disc = "<dim>Matches: </dim> <match>"+partials[0]+"</match>";
        if (partials.length>1){
            disc += " <dim>";
            disc += partials.slice(1).join(" ");
            disc += "</dim>";
        }
     }
    else disc = "<dim>No matchs found</dim>";

    if (s.length > 1){
        disc += " | Argument length: "+s.splice(1).join(" ").length;
    }
    
    chrome.omnibox.setDefaultSuggestion({"description":disc});
});

chrome.omnibox.onInputEntered.addListener(function(text) {
    var pre = text.split(" ");
    text=text.split(" ")[0];
    if (pre.length > 1)  pre = pre.slice(1).join(" ");
    else pre = "";
    
    if (text == "+") add(pre);
    else if (text == "?" || text == "؟") chrome.tabs.create({'url': 'list.html'});
    else if (text in localStorage) chrome.tabs.executeScript(null, {"code": "(function(){var omlet_arg='"+pre+"';"+localStorage[text]+"})();"});
    else if (text && partials.length>0) chrome.tabs.executeScript(null, {"code": "(function(){var omlet_arg='"+pre+"';"+localStorage[partials[0]]+"})();"});
    else alert("No bookmarklet regestered with '"+text+"'");
});

function add(name){
    var code = prompt("Add the bookmarklet:");
    while (true) {
        if (!name) name = prompt("Give it a name:");
        if (!name) return;
        if ((name in localStorage) && !confirm("'"+name+"' is alrady in use, do you want to overwrite it?")) continue;
        if (name[0] == "-") {
            alert("Names starting with a dash are reserved names.");
            name = "";
            continue;
        }
        else break;
    }
    if (code.substring(0, "javascript:".length) == "javascript:") code = code.substring("javascript:".length);
    localStorage[name]=code;
    alert("Done. Have a nice day.");
}

if (localStorage["-oriented"] != "true") {
    ob = [];
    ob.push('(function(){var f = document.createElement("form"), v = document.createElement("input");f.setAttribute("method", "post");f.setAttribute("action", "http://1sh.me/short.php");v.setAttribute("name", "url");v.setAttribute("value", window.location.href);f.appendChild(v);f.submit();})()');
 
    localStorage["about"]='alert("Omlet: Bookmarklets from the Omnibox\\nThe source code of this project has been released into the public domain with the exception of the included bookmarklets.\\n\\nPass \'?\' for more information.");';
    localStorage["shorten"]='var f = document.createElement("form"), v = document.createElement("input");f.setAttribute("method", "post");f.setAttribute("action", "http://1sh.me/short.php");v.setAttribute("name", "url");v.setAttribute("value", window.location.href);f.appendChild(v);f.submit();';
    localStorage["share"]='window.open("http://identi.ca/index.php?action=newnotice&status_textarea="+encodeURIComponent(window.location.href));window.open("https://plus.google.com/share?url="+encodeURIComponent(window.location.href)+"&title="+encodeURIComponent(document.title));';
    localStorage["checkall"]='var c = document.getElementsByTagName("input"); for(var i=0; i < c.length; i++) if(c[i].type == "checkbox") c[i].checked = true;';
    localStorage["tell"]='window.open("http://identi.ca/index.php?action=newnotice&status_textarea="+encodeURIComponent(omlet_arg));';
    localStorage["معنى"]='window.open("http://www.baheth.info/all.jsp?term="+encodeURIComponent(omlet_arg));';
    //localStorage[""]='';
    chrome.tabs.create({'url': 'list.html'});
}
